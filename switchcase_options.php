<?php

/**
 * Balises SPIP génériques supplémentaires, du genre Bonux
 *
 * @copyright  2015-2016
 * @author     JLuc chez no-log.org
 * @licence    GPL
 */

// gestion du niveau d'imbrication
function switchcase_niveau($add = false) {
	static $niveau = 0;
	if ($add) $niveau += $add;
	if ($niveau < 0) $niveau = 0;
	return $niveau;
}
	
function balise_SWITCH_dist($p) {
	$_val = interprete_argument_balise(1, $p);
	if ($_val === NULL) {
		// sans argument, renvoie la valeur testée (ou vide hors contexte)
		$p->code = "(isset(\$Pile['vars']['_switch_']) ? \$Pile['vars']['_switch_'] : '')";
//		$err = array('zbug_balise_sans_argument', array('balise' => ' #SWITCH'));
//		erreur_squelette($err, $p);
	}
	else {
		$p->code = 
			  "(vide(\$Pile['vars']['_switch_'] = \$Pile['vars']['_switch_'.switchcase_niveau(1)] = $_val)"
			. ".vide(\$Pile['vars']['_switch_matched_'] = \$Pile['vars']['_switch_matched_'.switchcase_niveau()] = ''))";
		// #GET{_switch_} renvoie maintenant la valeur testée
		// et #GET{_switch_matched_} indique si un test #CASE a déjà été satisfait
	}
	$p->interdire_script = false;
	return $p;
}

function balise_CASE_dist($p) {
	$tested = interprete_argument_balise(1, $p);
	if ($tested === NULL) {
		// sans argument, renvoie ' ' si la valeur a été trouvée, '' sinon
		$p->code = "(isset(\$Pile['vars']['_switch_matched_']) ? \$Pile['vars']['_switch_matched_'] : '')";
		// $err = array('zbug_balise_sans_argument', array('balise' => ' #CASE'));
		// erreur_squelette($err, $p);
	}
	else {
		$p->code = "(($tested == \$Pile['vars']['_switch_'])"
			. " ? ' '.vide(\$Pile['vars']['_switch_matched_'] = \$Pile['vars']['_switch_matched_'.switchcase_niveau()] = ' ')"
			. " : '')";
	}; 
	$p->interdire_script = false;
	return $p;
}

function balise_CASE_DEFAULT_dist($p) {
	$p->code = "(\$Pile['vars']['_switch_matched_'] ? '' : ' ')";
	$p->interdire_script = false;
	return $p;
}

function balise_SWITCH_END_dist($p) {
	$p->code = 
			"(!switchcase_niveau(-1) ? vide(\$Pile['vars']['_switch_'] = \$Pile['vars']['_switch_matched_'] = '')"
		. " : (vide(\$Pile['vars']['_switch_'] = \$Pile['vars']['_switch_'.switchcase_niveau()])"
		. ".vide(\$Pile['vars']['_switch_matched_'] = \$Pile['vars']['_switch_matched_'.switchcase_niveau()])))";
	$p->interdire_script = false;
	return $p;
}